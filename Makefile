IMAGE?=$(shell realpath $(shell pwd)/image)

all:	$(addsuffix .html,$(basename $(shell find -type f -name '*.mkd')))
	@echo $?

%.html:	%.mkd head.part.tmpl foot.part.tmpl CC_3.0_Attribution_ShareAlike.part.tmpl
	@echo "Generating $@ ..."
	@ruby -e "File.open('$<.tmp', 'w') {|file| file.write File.read('$<').gsub(/#\{include ([0-9A-Za-z_.]+)\}/) {|z| File.read(\"#{\$$1}.part.tmpl\")}}"
	@sed -i -e '1 s,^Title: ,Title: Exherbo - ,' $<.tmp
	@sed -i -e 's/^Description: (.*)$/<meta name="description" content="\0" />/' $<.tmp
	@sed -i -r -e 's|`\[pkg:(.*)\]|`\[escapedpkg:\1\]|g' $<.tmp
	@sed -i -r -e 's|`\[::(.*)\]|`\[:repo:\1\]|g' $<.tmp
	@sed -i -r -e 's|\[pkg:repository/(.+)\]|\[`repository/\1`\](//git.exherbo.org/summer/repositories/\1/index.html)|g' $<.tmp
	@sed -i -r -e 's|\[pkg:(.+/.+)\]|\[`\1`\](//git.exherbo.org/summer/packages/\1/index.html)|g' $<.tmp
	@sed -i -r -e 's,\[::(\w+)\],\[`::\1`\](//git.exherbo.org/\1.git),g' $<.tmp
	@sed -i -r -e 's|`\[:repo:(.+)\]|`\[::\1\]|g' $<.tmp
	@sed -i -r -e 's|`\[escapedpkg:(.+)\]|`\[pkg:\1\]|g' $<.tmp
	@maruku --html --output $@ $<.tmp
	@sed -i -e 's,<head>,&<link rel="shortcut icon" href="/favicon.png" />,' $@
	@sed -i -e 's,<td />,<td></td>,g' $@
	@sed -i -e 's,</nav>,</nav><div class="container">,;s,</body>,</div></body>,' $@
	@sed -i -e 's,<html.*xml:,<html ,' $@
	@sed -i -e '/^<?xml/ { N;N;N;d; };s/<html /<!DOCTYPE html>\n<html /' $@
	@sed -i -e 's,<table>,<table class="table">,g' $@
	@sed -i -e 's,</head>,<meta name="viewport" content="width=device-width\, initial-scale=1\, maximum-scale=1\, user-scalable=no" /><link rel="shortcut icon" href="/favicon.ico" /></head>,' $@
	@rm $<.tmp

deploy: all
	@[[ -d "$(IMAGE)" ]] || mkdir -p "$(IMAGE)"
	@rsync -a . "$(IMAGE)" \
		--delete-after \
		--exclude "$(IMAGE)" \
		--exclude image \
		--exclude '*.git*' \
		--exclude '*.mkd' \
		--exclude 'Makefile' \
		--exclude '*.tmp'

clean:
	rm -rf {,*/}*.tmp {,*/}*.html

.phony:	clean
