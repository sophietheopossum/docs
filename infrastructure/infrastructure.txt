
On tchaikovsky

- WWW
  Ask: kloeri
  www.git and docs.git - cron job under the 'arbor' user for updating
  automatically.

- Git
  Ask: zlin
  Lives in /srv/git/ - under the 'git' user
  Controlled by gitolite - config in gitolite-admin.git
  Hooks are in git-hooks.git
  We support both ssh:// (for pushing), git:// (using git-daemon) and http://
  (using apache).
  We enabled user-path which means that any user can put git repositories in
  ~/public_git/ and they will be accessible as git://git.exherbo.org/~user/foo.git

  Gitolite 3 was installed by cloning a tag of git://github.com/sitaramc/gitolite and running:
  # gitolite/install -to $HOME/bin

  Our .gitolite.rc has the following changes:

  UMASK                           =>  0022,
  GIT_CONFIG_KEYS                 =>  'cgit.* hooks.* gitweb.* irker.*',
  SAFE_CONFIG => {
      HASH                        => '#',
  },
  POST_COMPILE =>
      [
          'cgitrc-section'
      ],
  LOCAL_CODE                  =>  "$ENV{HOME}/.gitolite/local",
  ENABLE =>
      [
          # updates 'description' file instead of 'gitweb.description' config item
          'cgit',
      ],

  To make changes take effect, to run triggers or to update hooks run as git:
  # gitolite setup

  Our trigger and all our hooks are found in gitolite-admin.git local/

  To take gitolite down for maintenance run this in ~git/
  # echo 'Down because blah' > .gitolite.down

- Cgit
  Ask: zlin, somasis
  Lives in /srv/www/git.exherbo.org/cgit/
  Configuration files consist of /etc/cgitrc and part of /etc/apache2/vhosts.d/50_git.exherbo.org.conf
  We let gitolite set the description, owner and section of each repository and
  let gitolite pick it all up along with which repositories are exported using
  scan-path. We abuse how it emulates gitweb for how to read those. Exporting
  repositories is controlled via the daemon user in gitolite.

  The cgit configuration files et al live in www.git in the cgit dir, which gets
  deployed automatically as part of WWW updating.
- ssh
  Ask: kloeri
- Mail
  Ask: kloeri
- Summer
  Ask: zlin
  Hourly cron job under the 'arbor' user. Runs make in summer.git and produces
  mails once every 24 hours as well as whenever an error occurs. Reuses the
  repository configuration of Playboy.
  Uploaded to /srv/www/git.exherbo.org/summer/ where it becomes available as
  http://summer.exherbo.org/ which is an alias for
  http://git.exherbo.org/summer/.
  It also creates packages.json (available at https://git.exherbo.org/summer/packages.json)
  which is consumed by repology.
- Playboy
  Ask: zlin
  Three hourly cron jobs under the 'arbor' user. Runs a script in
  infra-scripts.git under playboy/. Once for producing
  unavailable.tar.gz, once for unavailable-unofficial.tar.gz and once for
  layman-repositories.tar.gz (for unavailable in Gentoo). Tarballs are uploaded
  to /srv/www/git.exherbo.org/cgit/ where it becomes available as
  http://git.exherbo.org/${tarball}.
- Lists
  Ask: zlin
  We use mailman. A list of all our mailing lists can be seen on:
  http://lists.exherbo.org/mailman/listinfo/
  Its cron jobs live under the 'mailman' user.
  The 2.x version we currently use still needs python2.
- u-u-commits
  Ask: sardemff7
  Uses eventd and git-eventc
  http://www.eventd.org/ http://git-eventc.eventd.org/
  Currently supports GitHub WebHook, Gitorious WebHook and self-hosted
  repository (using a GitHub WebHook stub in Bash)
  GitHub URL: https://bugs.exherbo.org/github-eventc?project=Exherbo&token=<token>
  Gitorious URL: https://bugs.exherbo.org/github-eventc?project=Exherbo&service=gitorious&token=<token>
  GitHub WebHook stub in Bash: https://gist.github.com/sardemff7/8770751
  Token is written in /home/sardemff7/u-u-commits-token
- Planet
  Cron job runs 'generate.sh' every 20 minutes under the 'arbor' user and
  picks up changes to configs/planet-exherbo.ini in git.exherbo.org/planet.
  ATTENTION: This planet thingy needs python2!

On distfiles.exherbo.org (owned by kimrhh)

- Distfiles
  Ask: kimrhh, somasis
  Uses a cron script in infra-scripts.git under accerso/.
  We mirror distfiles for official, dev, and unofficial repositories. We manually
  maintain a list of archives that must not be mirrored because paludis does not
  properly support RESTRICT=mirror or whatever the relevant DOWNLOADS label should be.

On stages.exherbo.org (owned by kimrhh)

- nightlies (Paludis)
  Ask: zlin
  Uses the 'nightlies' script in infra-scripts.git. This runs as the
  'nightlies' user in a cron job every night.
- stages
  Ask: zlin, philantrop
  Uses the 'make-exherbo-stages' script in infra-scripts.git. It supports only
  x86_64 (aka amd64) and i686 (aka x86). It gets run from a previous stage for
  the same architecture that you want to produce and uses the chroot support of
  paludis.

  There is currently no automatic stage building in place.
