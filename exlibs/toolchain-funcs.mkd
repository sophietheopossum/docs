Title: toolchain-funcs.exlib
CSS: /css/main.css

#{include head}

# toolchain-funcs.exlib

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

`toolchain-funcs.exlib` is an exlib used to get common information about the
toolchain being used to compile the package. This includes things such as
the C compiler being used (gcc/clang), GCC version, and so on.

## Functions

### `cc-has-defined()`, `cxx-has-defined()`

Used to check if the C/CXX compiler at `$CC`/`$CXX` has a certain value defined.

It runs the compiler in question in a mode that ensures that it only dumps the
internal definitions that are passed to all things the compiler compiles, and
it contains some definitions such as the OS type, architecture type, the
compiler implementation, and other details which may be desired by source code
being compiled.

#### Usage

`cc-has-defined __clang__ && echo compiling with clang.`

`cxx-has-defined __clang__ && echo compiling with clang++.`

### `cc-is-clang()`, `cc-is-gcc()`, `cxx-is-clang()`, `cxx-is-gcc()`

Used to check what implementation we support as system compiler is being used.

These are just simple checks that take advantage of the aforementioned
`cc-has-defined`/`cxx-has-defined` functions to check for definitions which
are set by `clang` and `gcc`.

#### Usage

`! cc-is-gcc && die "This package should only be compiled with gcc."`

`! cxx-is-gcc && die "This package should only be compiled with g++."`

### `gcc-fullversion()`, `gxx-fullversion()`

Returns the full version of the C/CXX compiler being used.

#### Example Output

    5.2.0

#### Usage

    if [[ $(gcc-fullversion) == 5.2.0 ]];then
        echo "Wow, you've got a shiny new compiler there"
    fi

### `gcc-version()`, `gxx-version()`

Basically the same as `gcc-fullversion`, but only outputs the major and minor
versions.

#### Example Output

    5.2

### `gcc-major-version()`, `gxx-major-version()`

Basically the same as `gcc-version`, but only returns the major.

### `gcc-minor-version()`, `gxx-major-version()`

Basically the same as `gcc-version`, but only returns the minor.

### `gcc-major-version()`, `gxx-major-version()`

Returns only the micro (the 0 in 5.2.0) from `gcc-fullversion`.

--
Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
